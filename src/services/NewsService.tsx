import axios from 'axios';

const API_URL = 'https://hn.algolia.com/api/v1/search_by_date';

type paramRequest = {
  query: string | undefined;
  page: number;
};

/**
 * Get news from api filters by query and page
 *
 * @param {string}   query        Type of news by framework or library (angular,reactjs or vuejs).
 * @param {number}   page         Number of page. Start in 0.
 *
 * @return {object} Return array of news.
 */
export const getNews = async ({ query, page }: paramRequest) => {
  try {
    const response = await axios.get(`${API_URL}?query=${query}&page=${page}`);
    return response?.data;
  } catch (error: any) {
    console.info('My custom error', error.response.data.error);
  }
};
