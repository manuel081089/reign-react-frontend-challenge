import { News } from '../components/News/News.model';

export type ApiResponse = {
  exhaustiveNbHits: boolean;
  exhaustiveTypo: boolean;
  hits: Array<News>;
  hitsPerPage: number;
  nbHits: number;
  nbPages: number;
  page: number;
  params: string;
  processingTimeMS: number;
  query: string;
  renderingContent: any;
};
