import { css } from '@emotion/react';
import { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { ClipLoader } from 'react-spinners';
import ButtonGroup from '../components/ButtonGroup/ButtonGroup';
import Header from '../components/Header';
import { News } from '../components/News/News.model';
import NewsList from '../components/News/NewsList';
import Pagination from '../components/Pagination/Pagination';
import {
  DEFAULT_PAGINATION,
  PaginationModel,
} from '../components/Pagination/Pagination.model';
import CustomCombobox from '../components/Shared/CustomCombobox';
import { ComboboxItem } from '../components/Shared/CustomCombobox.model';
import { comboboxData } from '../data/combobox.data';
import { ApiResponse } from '../services/ApiResponse.model';
import { getNews } from '../services/NewsService';

const NewsPage = () => {
  const [query, setQuery] = useState<string | undefined>();
  const [loadingNews, setLoadingNews] = useState<boolean>(false);
  const [news, setNews] = useState<Array<News>>([]);
  const [favoriteNews, setFavoriteNews] = useState<Array<News>>(
    JSON.parse(localStorage.getItem('favoriteNews') || '[]')
  );
  const comboboxItems: ComboboxItem[] = comboboxData;
  const [selectedFavorites, setSelectedFavorites] = useState<boolean>(false);
  const [pagination, setPagination] =
    useState<PaginationModel>(DEFAULT_PAGINATION);
  const [infinityScroll, setInfinityScroll] = useState<boolean>(false);

  const override = css`
    display: block;
    margin: 0 auto;
  `;

  useEffect(() => {
    if (query && !selectedFavorites) {
      setPagination(DEFAULT_PAGINATION);
      fetchNews(query, 0, true);
    } else {
      if (query) {
        const auxFavoriteNews = JSON.parse(
          localStorage.getItem('favoriteNews') || '[]'
        );
        setFavoriteNews(
          auxFavoriteNews.filter((favNews: News) => favNews.query === query)
        );
      }
    }
  }, [query, selectedFavorites, infinityScroll]);

  useEffect(() => {
    const auxNews = asyncNewsWithFavoriteNews(news);
    setNews(auxNews);
  }, [favoriteNews]);

  const fetchNews = async (
    query: string | undefined,
    page: number,
    changeQuery: boolean = false
  ) => {
    try {
      setLoadingNews(true);
      const response: ApiResponse = await getNews({ query, page });

      // Add query field to be able to separate the favorite news
      // without making a request to the api
      const newsWithQuery = response?.hits?.map((item: News) => ({
        ...item,
        query,
      }));
      const auxNews = asyncNewsWithFavoriteNews(newsWithQuery);

      // Add items in infinite scroll or change items in classic scroll
      infinityScroll && !changeQuery
        ? setNews((prevNews: Array<News>) => [...prevNews, ...auxNews])
        : setNews(auxNews);

      setPagination({
        page: response.page + 1,
        totalPages: response.nbPages,
        totalItems: response.nbHits,
        pageSize: response.hitsPerPage,
      });
      setLoadingNews(false);
    } catch (error: any) {
      setLoadingNews(false);
      console.log(error);
    }
  };

  const handleChangeFavorite = (item: News) => {
    let auxFavoriteNews: Array<News> = JSON.parse(
      localStorage.getItem('favoriteNews') || '[]'
    );

    item.favorite
      ? auxFavoriteNews.push(item)
      : (auxFavoriteNews = auxFavoriteNews.filter(
          (news: News) => news.objectID !== item.objectID
        ));

    localStorage.setItem('favoriteNews', JSON.stringify(auxFavoriteNews));
    setFavoriteNews(auxFavoriteNews);
  };

  const asyncNewsWithFavoriteNews = (itemsNews: Array<News>) => {
    const favNews = JSON.parse(localStorage.getItem('favoriteNews') || '[]');
    return itemsNews.map((item: News) => ({
      ...item,
      favorite: !!favNews.find(
        (favorite: News) => favorite.objectID === item.objectID
      ),
    }));
  };

  return (
    <div>
      <Header title='HACKER NEWS' />
      <div className='container'>
        <div className='mt-5'>
          <ButtonGroup
            selectedFavorites={selectedFavorites}
            onChange={(value: boolean) => setSelectedFavorites(value)}
          />
        </div>
        <div className='d-flex mt-5'>
          <CustomCombobox
            items={comboboxItems}
            onChange={(value) => setQuery(value)}
          />
          <button
            onClick={() => setInfinityScroll(!infinityScroll)}
            className='custom-button ml-2'
          >
            {infinityScroll ? 'Infinity' : 'Classic'}
          </button>
        </div>
        <div className='mt-5'>
          {!infinityScroll ? (
            <>
              <NewsList
                news={selectedFavorites ? favoriteNews : news}
                loading={loadingNews}
                onChangeFavorite={(item) => handleChangeFavorite(item)}
              />

              <div className='d-flex justify-content-center pagination-margins'>
                {!!pagination.totalPages && !selectedFavorites && (
                  <Pagination
                    page={pagination.page}
                    totalPages={pagination.totalPages}
                    prevPage={() => fetchNews(query, pagination.page - 2)}
                    nextPage={() => fetchNews(query, pagination.page)}
                    changePage={(pageNumber: number) =>
                      fetchNews(query, pageNumber - 1)
                    }
                  />
                )}
              </div>
            </>
          ) : (
            <InfiniteScroll
              dataLength={selectedFavorites ? favoriteNews.length : news.length}
              next={() => fetchNews(query, pagination.page + 1)}
              hasMore={true}
              loader={
                <ClipLoader
                  color='#1890ff'
                  loading={loadingNews}
                  css={override}
                  size={50}
                />
              }
            >
              <NewsList
                news={selectedFavorites ? favoriteNews : news}
                loading={false}
                onChangeFavorite={(item) => handleChangeFavorite(item)}
              />
            </InfiniteScroll>
          )}
        </div>
      </div>
    </div>
  );
};

export default NewsPage;
