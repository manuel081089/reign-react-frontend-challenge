import Select, { SingleValue } from 'react-select';
import { ComboboxItem, CustomComboboxProps } from './CustomCombobox.model';

const CustomCombobox = ({ items, onChange }: CustomComboboxProps) => {
  return (
    <>
      <Select
        isSearchable={false}
        isClearable={true}
        className='custom-select'
        onChange={(value: SingleValue<ComboboxItem>) => onChange(value?.id)}
        placeholder={<span className='custom-option'>Select your news</span>}
        formatOptionLabel={formatOptionLabel}
        getOptionValue={(option) => option.name}
        options={items}
      />
    </>
  );
};

const formatOptionLabel = ({ name, id, image }: ComboboxItem) => (
  <div className='d-flex align-items-center'>
    <img className='custom-image' src={image} alt={id} id={id} />
    <span className='custom-option'>{name}</span>
  </div>
);

export default CustomCombobox;
