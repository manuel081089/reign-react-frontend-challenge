export type ComboboxItem = {
  name: string;
  id: string;
  image: string;
};

export type CustomComboboxProps = {
  items: ComboboxItem[];
  onChange: (value: string | undefined) => void;
};
