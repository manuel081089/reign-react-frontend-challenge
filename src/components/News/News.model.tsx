export type News = {
  author?: string;
  comment_text?: string;
  created_at?: string;
  created_at_i?: number;
  num_comments?: any;
  objectID?: string;
  parent_id?: number;
  points?: any;
  story_id?: number;
  story_text?: string;
  story_title?: string;
  story_url?: string;
  title?: string;
  url?: string;
  favorite?: boolean;
  query?: string;
};
