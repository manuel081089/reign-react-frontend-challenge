import moment from 'moment';
import clock from '../../assets/images/clock.svg';
import heartFavorite from '../../assets/images/heart-favorite.png';
import heart from '../../assets/images/heart.png';
import { News } from './News.model';

type NewsItemProps = {
  item: News;
  changeFavorite: (value: boolean) => void;
};

const NewsItem = ({ item, changeFavorite }: NewsItemProps) => {
  const openInNewTab = (url: string | undefined) => {
    window?.open(url, '_blank')?.focus();
  };

  return (
    <div className='news-item-container d-flex justify-content-between'>
      <div
        className='news-item-text-container'
        onClick={() => openInNewTab(item.story_url)}
      >
        <div className='d-flex align-items-center'>
          <img src={clock} alt='clock' />
          <span className='news-item-time'>
            {moment(item.created_at).fromNow()} by {item.author}
          </span>
        </div>
        <div className='mt-2'>
          <span className='news-item-title'>{item.story_title}</span>
        </div>
      </div>
      <div
        className='news-item-favorite-container h-100 d-flex justify-content-center align-items-center'
        onClick={() => changeFavorite(!item.favorite)}
      >
        {item.favorite ? (
          <img className='news-item-heart' src={heartFavorite} alt='heart' />
        ) : (
          <img className='news-item-heart' src={heart} alt='heart-favorite' />
        )}
      </div>
    </div>
  );
};

export default NewsItem;
