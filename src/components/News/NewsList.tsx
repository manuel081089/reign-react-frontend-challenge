import { css } from '@emotion/react';
import ClipLoader from 'react-spinners/ClipLoader';
import { News } from './News.model';
import NewsItem from './NewsItem';

type NewsListProps = {
  news: Array<News>;
  loading: boolean;
  onChangeFavorite: (item: News) => void;
};

const NewsList = ({ news, loading, onChangeFavorite }: NewsListProps) => {
  const handleChangeFavorite = (item: News) => {
    onChangeFavorite({ ...item, favorite: !item.favorite });
  };

  const override = css`
    display: block;
    margin: 0 auto;
  `;

  return (
    <>
      {loading ? (
        <ClipLoader
          color='#1890ff'
          loading={loading}
          css={override}
          size={100}
        />
      ) : (
        <div className='row custom-margin--35'>
          {news
            .filter(
              (item: News) =>
                !!item.story_title && !!item.story_url && !!item.created_at
            )
            .map((item: News, index: number) => (
              <div key={index} className='col-lg-6 col-md-6 col-sm-12'>
                <NewsItem
                  changeFavorite={() => handleChangeFavorite(item)}
                  item={item}
                />
              </div>
            ))}

          {news.filter(
            (item: News) =>
              !!item.story_title && !!item.story_url && !!item.created_at
          ).length === 0 && (
            <div className='news-no-item'>
              <h3 className='news-no-item-text'>
                There is no news for this section. Please select another
                section.
              </h3>
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default NewsList;
