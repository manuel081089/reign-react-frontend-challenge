type HeaderProps = {
  title: string;
};

const Header = ({ title }: HeaderProps) => {
  return (
    <div className='header-container'>
      <div className='container'>
        <span className='header-title'>{title}</span>
      </div>
    </div>
  );
};

export default Header;
