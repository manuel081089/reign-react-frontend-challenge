import ButtonGroupItem from './ButtonGroupItem';

type ButtonGroupProps = {
  selectedFavorites: boolean;
  onChange: (value: boolean) => void;
};

const ButtonGroup = ({ selectedFavorites, onChange }: ButtonGroupProps) => {
  return (
    <div className='d-flex justify-content-center'>
      <ButtonGroupItem
        active={!selectedFavorites}
        title='All'
        styleClass={
          selectedFavorites
            ? 'news-button-border'
            : 'news-button-border-left-active'
        }
        onActiveChange={() => onChange(false)}
      />
      <ButtonGroupItem
        active={selectedFavorites}
        title='My faves'
        styleClass={
          selectedFavorites
            ? 'news-button-border-right-active'
            : 'news-button-border'
        }
        onActiveChange={() => onChange(true)}
      />
    </div>
  );
};

export default ButtonGroup;
