type ButtonGroupItemProps = {
  title: string;
  active?: boolean;
  styleClass: string;
  onActiveChange: () => void;
};
const ButtonGroupItem = ({
  title,
  active,
  styleClass,
  onActiveChange,
}: ButtonGroupItemProps) => {
  return (
    <button
      onClick={() => onActiveChange()}
      className={
        active
          ? `news-button-active ${styleClass}`
          : `news-button ${styleClass}`
      }
    >
      {title}
    </button>
  );
};

export default ButtonGroupItem;
