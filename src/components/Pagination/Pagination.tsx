type PaginationProps = {
  page: number;
  totalPages: number;
  offset?: number;
  prevPage: () => void;
  nextPage: () => void;
  changePage: (page: number) => void;
};

const Pagination = ({
  page,
  totalPages,
  prevPage,
  nextPage,
  changePage,
  offset = 5,
}: PaginationProps) => {
  const minVal = page - offset > 1 ? page - offset : 0;
  const maxVal = page + offset < totalPages ? page + offset : totalPages;

  const createPaginationButtons = () => {
    const buttoms: any[] = [];
    for (let index = minVal; index < maxVal; index++) {
      buttoms.push(
        <button
          onClick={() => changePage(index + 1)}
          key={index}
          className={`pagination-button ${page === index + 1 ? 'active' : ''}`}
        >
          {index + 1}
        </button>
      );
    }

    return buttoms;
  };

  return (
    <div className='d-flex flex-wrap'>
      <button
        disabled={page === 1}
        onClick={prevPage}
        className='pagination-button'
      >
        &#60;
      </button>
      {createPaginationButtons()}
      <button
        disabled={page === totalPages}
        onClick={nextPage}
        className='pagination-button'
      >
        &#62;
      </button>
    </div>
  );
};

export default Pagination;
