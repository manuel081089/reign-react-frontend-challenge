export type PaginationModel = {
  page: number;
  pageSize: number;
  totalItems: number;
  totalPages: number;
};

export const DEFAULT_PAGINATION: PaginationModel = {
  page: 0,
  pageSize: 20,
  totalItems: 0,
  totalPages: 0,
};
