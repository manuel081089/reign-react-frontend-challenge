import angularLogo from '../assets/images/angular.png';
import reactLogo from '../assets/images/reactjs.png';
import vueLogo from '../assets/images/vuejs.png';
import { ComboboxItem } from '../components/Shared/CustomCombobox.model';

export const comboboxData: ComboboxItem[] = [
  {
    id: 'angular',
    name: 'Angular',
    image: angularLogo,
  },
  {
    id: 'reactjs',
    name: 'Reactjs',
    image: reactLogo,
  },
  {
    id: 'vuejs',
    name: 'Vuejs',
    image: vueLogo,
  },
];
